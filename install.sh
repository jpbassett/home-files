#!/bin/bash

rootdir="$(dirname "$(readlink -m "$0")")"


BLUE="\e[34m"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
NC="\e[39m"
GRAY="\e[37m"

width=60
pad=$(printf ".%.0s" {1..60})

function joinlist
{
    local ret=$1
    local sep=$2
    local tmp=" "
    shift 2
    printf -v tmp "%s" "${@/#/${sep}}"
    printf -v "${ret}" "%s" "${tmp##${sep}}"
}
function HDR()
{
    local title
    joinlist title " -> " "$@"

    echo -n "${title} ${pad} " | head -c ${width}
}
function PASS()
{
    echo -e "[${GREEN} OK ${NC}]"
}
function FAIL()
{
    echo -e "[${RED}FAIL${NC}]"
}
function SKIP()
{
    echo -e "[${YELLOW}SKIP${NC}]"
}
function INST()
{
    echo -e "[${BLUE}INST${NC}]"
}
function UPDT()
{
    echo -e "[${BLUE}UPDT${NC}]"
}


function check()
{
    if [ -e "${dst}" ]; then
        if [ -h "${dst}" ]; then
            dstpath=$(readlink -m "$dst")
            if [ "$src" == "$dstpath" ]; then
                PASS
                return
            fi
        fi
    fi
    INST
}


install_file() {
	src=$1
    dst=$2

    HDR "$src"
    if [ -e "$dst" ]; then
        if [ -h "$dst" ]; then
            spath=$(readlink -m "$src")
            dpath=$(readlink -m "$dst")
            if [ "$spath" == "$dpath" ]; then
                PASS
                return 0
            else
                UPDT
                echo "  Destination is a link to another file."
                cp --backup=t --suffix='.BAK.' "$dst" "dst"
            fi
        elif [ -f "$dst" ]; then
            UPDT
            echo "  Destination is a file"
            if diff -q "$src" "$dst"; then
                echo "  Files are the same, replacing with a link"
            else
                echo "  Files are different, backing up and replacing"
                cp --backup=t --suffix='.BAK.' "$dst" "$dst"
            fi
        elif [ -d "$dst" ]; then
            FAIL
            echo "    Destination is a directory. I'm not touching that."
            return 1
        else
            FAIL
            echo "  I don't know what this filetype is"
            return 1
        fi
        rm -f "$dst"
        ln -s "$(readlink -m "$src")" "$dst"
    else
        UPDT
        echo "  Destination does not exist, creating"
        mkdir -pv "$(dirname "$dst")"
        ln -s "$(readlink -m "$src")" "$dst"
    fi
}

# Install all files in the home directory
echo "=== Installing home area files ==="
pushd "${rootdir}/home" &>/dev/null
for file in $(find . -xtype f | sort); do
    filename=$(basename "$file")
    srcpath=$(dirname "$file")

    src=$(readlink -m "$file")
    dst=${HOME}/${srcpath}/${filename}

    install_file "$src" "$dst"
done
popd &>/dev/null

# Special files are handled in seperate functions down here

echo "  "
echo "=== Special Files ==="

HDR "Vim Plugins"
if vim +PlugInstall +qall ; then
    HDR "Vim Plugins"
    PASS
else
    HDR "Vim Plugins"
    FAIL
fi

# gitconfig
# The git configuration is included, and the user.name and user.email must be
# filled out
HDR .gitconfig
dst=$(readlink -m special/gitconfig)
if git config --list | grep -q "include.path=${dst}"; then
    PASS
else
    UPDT
    git config --global include.path "$(readlink -m special/gitconfig)"
fi

HDR Git Username
if git config --list | grep -q user.name; then
    PASS
else
    FAIL
    echo "  Set your git username!"
fi

HDR Git Email
if git config --list | grep -q user.email; then
    PASS
else
    FAIL
    echo "  Set your git username!"
fi
